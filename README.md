# README #

* This is a base project for CSS, HTML, Javascript and PHP training
* 0.0.1 Alpha
* Learn Markdown https://bitbucket.org/tutorials/markdowndemo

### Requirements ###

* PHP => 5.5 (XAMPP in Windows)
* MySQL Server (Included in XAMPP)
* Text editor (Sublime Text/ Atom) or IDE (NetBeans, Eclipse or PHP Storm)
* Git
* Some patience

### Who do I talk to? ###

* Coded by Jason Jiménez Cruz: jasonjimenezcruz@gmail.com
