<?php

namespace AppBundle\Entity;

/**
 * Comment
 */
class Comment
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $author;

    /**
     * @var string
     */
    private $content;

    /**
     * @var \AppBundle\Entity\Entry
     */
    private $entry_id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Comment
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Comment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set entryId
     *
     * @param \AppBundle\Entity\Entry $entryId
     *
     * @return Comment
     */
    public function setEntryId(\AppBundle\Entity\Entry $entryId = null)
    {
        $this->entry_id = $entryId;

        return $this;
    }

    /**
     * Get entryId
     *
     * @return \AppBundle\Entity\Entry
     */
    public function getEntryId()
    {
        return $this->entry_id;
    }
}

