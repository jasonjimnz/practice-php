<?php
/**
 * Created by PhpStorm.
 * User: jasonjimenezcruz
 * Date: 28/4/17
 * Time: 17:56
 */

namespace AppBundle\Controller;


class BaseClass
{
    public $param1;

    public function __construct($param = null)
    {
        if($param){
            $this->param1 = (string) $param;
        } else {
            $this->param1 = "Sin parámetros";
        }
    }

    function __toString()
    {
        return $this->param1;
    }


}