<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Entry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
    * @Route("/hola/{name}", name="saludo")
    */
    public function saludoAction(Request $request, $name)
    {
        
        $data = array(
            'mensaje' => $name,
            'fecha' => new \DateTime()
        );
        return $this->render(
            'saludo.html.twig', 
            $data
        );
    }

    /**
    * @Route("/contador/xml/{limite}" , name="contadorXml")
    */
    public function contadorXmlAction(Request $request, $limite)
    {
        $data = array(
            'numbers' => array(),
            'errors' => false
            );
        if (is_numeric($limite)){
            for($x = 0; $x < $limite; $x++){
                $data['numbers'][] = $x;
            }
        } else {
            $data['error_msj'] = 'La variable límite tiene que ser un número entero';
            $data['errors'] = true;
        }

        $template = 'xml_base.xml.twig';

        $html = $this->container->get('templating')->render($template, $data);
        $response = new Response($html);
        $response->headers->set('Content-Type', 'xml');
        return $response;
    }

    /**
     * @Route("/contador/json/{limite}" , name="contadorJson")
     */
    public function contadorJsonAction(Request $request, $limite)
    {
        $data = array(
            'numbers' => array(),
            'errors' => false
        );
        if (is_numeric($limite)){
            for($x = 0; $x < $limite; $x++){
                $data['numbers'][] = $x;
            }
        } else {
            $data['error_msj'] = 'La variable límite tiene que ser un número entero';
            $data['errors'] = true;
        }

        $response = new JsonResponse();
        $response->setData($data);
        $response->setStatusCode(200);
        return $response;
    }

    /**
     * @Route("/entrada/crear" , name="crearEntrada")
     */
    public function createEntryAction(Request $request)
    {
        if($request->isMethod('POST')){
            $post = $request->request;
            $entry = new Entry();
            $entry->setTitle($post->get('title'));
            $entry->setCategory($post->get('category','default'));
            $entry->setContent($post->get('content',null));
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entry);
            $em->flush();

            return new Response('Guardado en el id: '. $entry->getId());
        } else {
            return new Response('Tienes que llamar por POST con title, category y content como parámetros');
        }
    }

    /**
     * @Route("/entrada/lista/{tipo}" , name="listaEntradas")
     */
    public function getEntriesAction(Request $request, $tipo = "xml")
    {
        $entries = $this->getDoctrine()->getEntityManager()->getRepository('AppBundle:Entry')->findAll();
        $entryList = array();
        foreach ($entries as $key => $val){
            $comments = $this->getDoctrine()->getEntityManager()->getRepository('AppBundle:Comment')->findBy(['entry_id' => $val->getId()]);
            $commentList = [];
            foreach ($comments as $c){
                $commentList[] = [
                    'id' => $c->getId(),
                    'author' => $c->getAuthor(),
                    'content' => $c->getContent()
                ];
            }
            $entryList[] = [
                'id' => $val->getId(),
                'content' => $val->getContent(),
                'category' => $val->getCategory(),
                'title' => $val->getTitle(),
                'comments' => $commentList
            ];
        }
        if(strtolower($tipo) == "xml"){
            $template = 'entry.xml.twig';
            $html = $this->container->get('templating')->render($template, ['entries' => $entryList]);
            $response = new Response($html);
            $response->headers->set('Content-Type', 'xml');
            return $response;

        } elseif(strtolower($tipo) == "json"){
            $response = new JsonResponse();
            $response->setData($entryList);
            $response->setStatusCode(200);
            return $response;
        } else {
            return new BadRequestHttpException("El tipo debe ser XML o JSON");
        }
    }
}
